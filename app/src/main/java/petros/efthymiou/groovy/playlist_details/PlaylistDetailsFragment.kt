package petros.efthymiou.groovy.playlist_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.fragment.navArgs
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_playlist.*
import kotlinx.android.synthetic.main.fragment_playlist_details.*
import petros.efthymiou.groovy.R

@AndroidEntryPoint
class PlaylistDetailsFragment : Fragment() {

    private val args: PlaylistDetailsFragmentArgs by navArgs()
    private val viewModel: PlaylistDetailsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_playlist_details, container, false)
        val id = args.playlistId

        observePlaylistDetails(id)
        observeLoader()

        return view
    }

    private fun observePlaylistDetails(id: String) {
        viewModel.getPlaylistDetails(id)
        viewModel.playlist.observe(this as LifecycleOwner) { playlistDetails ->
            if (playlistDetails.getOrNull() != null) {
                setupUI(playlistDetails)
            } else {
                Snackbar.make(
                    playlist_details_root,
                    R.string.generic_error_message,
                    Snackbar.LENGTH_LONG
                )
                    .show()
            }
        }
    }

    private fun observeLoader() {
        viewModel.loading.observe(this as LifecycleOwner) { isLoading ->
            when (isLoading) {
                true -> playlist_details_loader.visibility = View.VISIBLE
                else -> playlist_details_loader.visibility = View.GONE
            }
        }
    }

    private fun setupUI(playlistDetails: Result<PlaylistDetails>) {
        playlist_details_name.text = playlistDetails.getOrNull()!!.name
        playlist_details_description.text = playlistDetails.getOrNull()!!.description
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            PlaylistDetailsFragment().apply {

            }
    }
}