package petros.efthymiou.groovy.playlist_details

import retrofit2.http.GET
import retrofit2.http.Path

interface PlaylistDetailsAPI {
    @GET("playlist_details/{id}")
    suspend fun fetchPlaylistDetailsFromAPI(@Path("id") id: String): PlaylistDetails

}
