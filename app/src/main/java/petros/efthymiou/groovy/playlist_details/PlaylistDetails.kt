package petros.efthymiou.groovy.playlist_details

data class PlaylistDetails(
    val id: String,
    val name: String,
    val description: String
) {

}
