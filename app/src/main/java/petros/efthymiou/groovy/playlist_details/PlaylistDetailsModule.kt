package petros.efthymiou.groovy.playlist_details

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
class PlaylistDetailsModule {
    @Provides
    fun playlistDetailsAPI(retrofit: Retrofit): PlaylistDetailsAPI =
        retrofit.create(PlaylistDetailsAPI::class.java)
}