package petros.efthymiou.groovy.playlist_details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PlaylistDetailsViewModel @Inject constructor(
    val service: PlaylistDetailsService
) : ViewModel() {
    val loading = MutableLiveData<Boolean>()
    val playlist: MutableLiveData<Result<PlaylistDetails>> = MutableLiveData()

    fun getPlaylistDetails(id: String) {
        viewModelScope.launch {
            loading.postValue(true)
            service.fetchPlaylistDetails(id)
                .onEach {
                    loading.postValue(false)
                }
                .collect {
                    playlist.postValue(it)
                }
        }
    }


}