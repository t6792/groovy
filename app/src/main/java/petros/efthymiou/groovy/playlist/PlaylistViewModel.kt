package petros.efthymiou.groovy.playlist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.liveData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class PlaylistViewModel @Inject constructor(
    var repository: PlaylistRepository
) : ViewModel() {


    val loading = MutableLiveData<Boolean>()
    val playlists = liveData {
        loading.postValue(true)
        emitSource(repository.getPlaylists()
            .onEach {
                loading.postValue(false)
            }
            .asLiveData())
    }

}
