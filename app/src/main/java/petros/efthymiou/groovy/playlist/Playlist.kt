package petros.efthymiou.groovy.playlist

data class Playlist(
    val id: String,
    val name: String,
    val genre: String,
    val img: Int
)
