package petros.efthymiou.groovy.playlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_playlist.*
import kotlinx.android.synthetic.main.fragment_playlist.view.*
import petros.efthymiou.groovy.R

@AndroidEntryPoint
class PlaylistFragment : Fragment() {
    private val viewModel: PlaylistViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_playlist, container, false)

        observeLoader()
        observePlaylists()

        return view
    }

    private fun observeLoader() {
        viewModel.loading.observe(this as LifecycleOwner) { isLoading ->
            when (isLoading) {
                true -> loading_spinner.visibility = View.VISIBLE
                else -> loading_spinner.visibility = View.GONE
            }
        }
    }

    private fun observePlaylists() {
        viewModel.playlists.observe(this as LifecycleOwner, { playlists ->
            if (playlists.getOrNull() != null)
                setupRecyclerView(playlist_recycler_view, playlists.getOrNull()!!)
        }
        )
    }

    private fun setupRecyclerView(
        view: View?,
        playlists: List<Playlist>
    ) {
        with(view as RecyclerView) {
            layoutManager = LinearLayoutManager(context)

            adapter = PlaylistRecyclerViewAdapter(playlists) { id ->
                val action =
                    PlaylistFragmentDirections.actionPlaylistFragmentToPlaylistDetailsFragment(id)
                findNavController().navigate(action)
            }
        }
    }

    companion object {


        @JvmStatic
        fun newInstance() =
            PlaylistFragment().apply {

            }
    }
}