package petros.efthymiou.groovy.playlist_details

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Test
import petros.efthymiou.groovy.utils.BaseUnitTest
import petros.efthymiou.groovy.utils.captureValues
import petros.efthymiou.groovy.utils.getValueForTest

@ExperimentalCoroutinesApi
class PlaylistDetailsViewModelShould : BaseUnitTest() {

    lateinit var viewModel: PlaylistDetailsViewModel
    private val service: PlaylistDetailsService = mock()
    private val playlistDetails: PlaylistDetails = mock()
    private val expected = Result.success(playlistDetails)
    private val id = "1"
    private val exception = RuntimeException("Something went wrong")

    @Test
    fun getPlaylistDetailsFromTheService() = runBlockingTest {
        mockSuccessfulCase()
        viewModel.getPlaylistDetails(id)
        viewModel.playlist.getValueForTest()
        verify(service, times(1)).fetchPlaylistDetails(id)
    }

    @Test
    fun emitPlaylistDetailsFromService() = runBlockingTest {
        mockSuccessfulCase()
        viewModel.getPlaylistDetails(id)
        assertEquals(expected, viewModel.playlist.getValueForTest())
    }

    @Test
    fun emitErrorWhenErrorReceivedFromService() = runBlockingTest {
        mockFailureCase()
        viewModel.getPlaylistDetails(id)
        assertEquals(exception, viewModel.playlist.getValueForTest()!!.exceptionOrNull())
    }

    @Test
    fun showSpinnerWhileLoading() = runBlockingTest {
        mockSuccessfulCase()

        viewModel.loading.captureValues {
            viewModel.getPlaylistDetails(id)
            viewModel.playlist.getValueForTest()
            assertEquals(true, values[0])
        }

    }

    @Test
    fun hisSpinnerAfterPlaylistDetailsLoaded() = runBlockingTest {
        mockSuccessfulCase()

        viewModel.loading.captureValues {
            viewModel.getPlaylistDetails(id)
            viewModel.playlist.getValueForTest()
            assertEquals(false, values.last())
        }
    }

    private suspend fun mockSuccessfulCase() {
        runBlockingTest {
            whenever(service.fetchPlaylistDetails(id))
                .thenReturn(flow {
                    emit(expected)
                })
        }

        viewModel = PlaylistDetailsViewModel(service)
    }

    private suspend fun mockFailureCase() {
        runBlockingTest {
            whenever(service.fetchPlaylistDetails(id))
                .thenReturn(flow {
                    emit(Result.failure<PlaylistDetails>(exception = exception))
                })
        }

        viewModel = PlaylistDetailsViewModel(service)
    }
}