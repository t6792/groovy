package petros.efthymiou.groovy.playlist_details

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Test
import petros.efthymiou.groovy.utils.BaseUnitTest
import java.lang.RuntimeException

@ExperimentalCoroutinesApi
class PlaylistDetailsServiceShould : BaseUnitTest() {

    private val api: PlaylistDetailsAPI = mock()
    private val playlistDetails: PlaylistDetails = mock()
    private val id = "1"

    @Test
    fun fetchPlaylistDetailsFromAPI() = runBlockingTest {
        val service = PlaylistDetailsService(api)
        service.fetchPlaylistDetails(id).first()
        verify(api, times(1)).fetchPlaylistDetailsFromAPI(id)
    }

    @Test
    fun convertValuesToFlowResultAndEmitThem() = runBlockingTest {
        val service = mockSuccessfulCase()

        assertEquals(Result.success(playlistDetails), service.fetchPlaylistDetails(id).first())
    }

    @Test
    fun emitErrorResultWhenNetworkFails() = runBlockingTest {
        val service = mockFailureCase()

        assertEquals(
            "Something went wrong",
            service.fetchPlaylistDetails(id).first().exceptionOrNull()?.message
        )
    }

    private fun mockFailureCase(): PlaylistDetailsService {
        runBlockingTest {
            whenever(api.fetchPlaylistDetailsFromAPI(id))
                .thenThrow(RuntimeException("Damn backend devs..."))
        }
        return PlaylistDetailsService(api)
    }

    private fun mockSuccessfulCase(): PlaylistDetailsService {
        runBlockingTest {
            whenever(api.fetchPlaylistDetailsFromAPI(id))
                .thenReturn(playlistDetails)
        }
        return PlaylistDetailsService(api)
    }

}