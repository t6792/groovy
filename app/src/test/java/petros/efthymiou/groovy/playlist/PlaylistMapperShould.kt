package petros.efthymiou.groovy.playlist

import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.assertEquals
import org.junit.Test
import petros.efthymiou.groovy.R
import petros.efthymiou.groovy.utils.BaseUnitTest

@ExperimentalCoroutinesApi
class PlaylistMapperShould : BaseUnitTest() {

    private val playlistRawRock = PlaylistRaw("1", "le name", "rock")
    private val playlistRaw = PlaylistRaw("1", "le name", "le genre")
    private val mapper = PlaylistMapper()
    private val playlists = mapper(listOf(playlistRaw))
    private val playlist = playlists.first()
    private val playlistRock = mapper(listOf(playlistRawRock)).first()

    @Test
    fun keepTheSameID() {
        assertEquals(playlistRaw.id, playlist.id)
    }

    @Test
    fun keepTheSameName() {
        assertEquals(playlistRaw.name, playlist.name)
    }

    @Test
    fun keepTheSameGenre() {
        assertEquals(playlistRaw.genre, playlist.genre)
    }

    @Test
    fun mapDefaultImageWhenPlaylistGenreIsNotRock() {
        assertEquals(R.mipmap.playlist, playlist.img)
    }

    @Test
    fun mapRockImageWhenPlaylistGenreIsRock() {
        assertEquals(R.mipmap.rock, playlistRock.img)
    }
}