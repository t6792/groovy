package petros.efthymiou.groovy

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.adevinta.android.barista.assertion.BaristaRecyclerViewAssertions.assertRecyclerViewItemCount
import com.adevinta.android.barista.assertion.BaristaVisibilityAssertions.assertDisplayed
import com.adevinta.android.barista.assertion.BaristaVisibilityAssertions.assertNotDisplayed
import com.adevinta.android.barista.internal.matcher.DrawableMatcher.Companion.withDrawable
import org.hamcrest.Matchers.allOf
import org.junit.Test
import org.junit.runner.RunWith
import petros.efthymiou.groovy.playlist.idlingResource


@RunWith(AndroidJUnit4::class)
class PlaylistFeature : BaseUiTest() {

    @Test
    fun displayScreenTitle() {
        assertDisplayed("Playlists")
    }

    @Test
    fun displaysListOfPlaylists() {

        assertRecyclerViewItemCount(R.id.playlist_recycler_view, 10)

        // check playlist_name of first item of recycler view
        onView(
            allOf(
                withId(R.id.playlist_name),
                isDescendantOfA(
                    nthChildOf(withId(R.id.playlist_recycler_view), 0)
                )
            )
        )
            .check(matches(withText("Hard Rock Cafe")))
            .check(matches(isDisplayed()))

        // check genre of first item of recycler view
        onView(
            allOf(
                withId(R.id.playlist_genre),
                isDescendantOfA(
                    nthChildOf(withId(R.id.playlist_recycler_view), 0)
                )
            )
        )
            .check(matches(withText("rock")))
            .check(matches(isDisplayed()))

        // check image of first item of recycler view
        onView(
            allOf(
                withId(R.id.playlist_img),
                isDescendantOfA(
                    nthChildOf(withId(R.id.playlist_recycler_view), 1)
                )
            )
        )
            .check(matches(withDrawable(R.mipmap.playlist)))
            .check(matches(isDisplayed()))
    }

    @Test
    fun displaysLoaderWhileFetchingPlaylists() {
        // To unregister while fetching
        IdlingRegistry.getInstance().unregister(idlingResource)
        assertDisplayed(R.id.loading_spinner)
    }

    @Test
    fun hidesLoaderAfterPlaylistsFetched() {

        assertNotDisplayed(R.id.loading_spinner)
    }

    @Test
    fun displaysRockImageForRockSongs() {

        onView(
            allOf(
                withId(R.id.playlist_img),
                isDescendantOfA(
                    nthChildOf(withId(R.id.playlist_recycler_view), 0)
                )
            )
        )
            .check(matches(withDrawable(R.mipmap.rock)))
            .check(matches(isDisplayed()))


        onView(
            allOf(
                withId(R.id.playlist_img),
                isDescendantOfA(
                    nthChildOf(withId(R.id.playlist_recycler_view), 3)
                )
            )
        )
            .check(matches(withDrawable(R.mipmap.rock)))
            .check(matches(isDisplayed()))
    }

    @Test
    fun navigateToDetailsScreen() {
        onView(
            allOf(
                withId(R.id.playlist_img),
                isDescendantOfA(
                    nthChildOf(withId(R.id.playlist_recycler_view), 0)
                )
            )
        )
            .perform(click())

        assertDisplayed(R.id.playlist_details_root)
    }


}