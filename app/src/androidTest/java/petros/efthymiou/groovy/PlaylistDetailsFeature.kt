package petros.efthymiou.groovy

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import com.adevinta.android.barista.assertion.BaristaVisibilityAssertions.assertDisplayed
import com.adevinta.android.barista.assertion.BaristaVisibilityAssertions.assertNotDisplayed
import com.adevinta.android.barista.assertion.BaristaVisibilityAssertions.assertNotExist
import org.hamcrest.Matchers
import org.junit.Test
import petros.efthymiou.groovy.playlist.idlingResource

class PlaylistDetailsFeature : BaseUiTest() {
    @Test
    fun displaysPlaylistNameAndDescription() {
        navigateToPlaylistDetails()
        assertDisplayed("Hard Rock Cafe")
        assertDisplayed("Rock your senses with this timeless signature vibe list. \n\n • Poison \n • You shook me all night \n • Zombie \n • Rock'n Me \n • Thunderstruck \n • I Hate Myself for Loving you \n • Crazy \n • Knockin' on Heavens Door")
    }

    @Test
    fun displaysLoadingSpinnerWhileFetchingThePlaylistDetails() {
        IdlingRegistry.getInstance().unregister(idlingResource)

        Thread.sleep(2000)
        navigateToPlaylistDetails()

        assertDisplayed(R.id.playlist_details_loader)
    }

    @Test
    fun hidesLoadingSpinner() {
        navigateToPlaylistDetails()
        assertNotDisplayed(R.id.playlist_details_loader)
    }

    @Test
    fun displaysErrorMessageWhenNetworkCallFails() {
        navigateToPlaylistDetails(positionInList = 1)

        assertDisplayed(R.string.generic_error_message)
    }

    @Test
    fun hidesSnackbarAfterThreeSeconds() {
        navigateToPlaylistDetails(positionInList = 1)
        Thread.sleep(3000)
        assertNotExist(R.string.generic_error_message)
    }

    private fun navigateToPlaylistDetails(positionInList: Int = 0) {
        onView(
            Matchers.allOf(
                ViewMatchers.withId(R.id.playlist_img),
                ViewMatchers.isDescendantOfA(
                    nthChildOf(ViewMatchers.withId(R.id.playlist_recycler_view), positionInList)
                )
            )
        )
            .perform(ViewActions.click())
    }
}